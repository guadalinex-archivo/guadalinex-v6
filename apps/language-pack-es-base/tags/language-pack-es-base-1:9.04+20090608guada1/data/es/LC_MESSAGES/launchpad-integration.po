# Spanish translation for launchpad-integration
# Copyright (c) (c) 2006 Canonical Ltd, and Rosetta Contributors 2006
# This file is distributed under the same license as the launchpad-integration package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2006.
#
msgid ""
msgstr ""
"Project-Id-Version: launchpad-integration\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2009-04-17 07:22+0000\n"
"PO-Revision-Date: 2009-05-09 21:35+0000\n"
"Last-Translator: Ricardo Pérez López <ricpelo@gmail.com>\n"
"Language-Team: Spanish <es@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2009-05-19 00:38+0000\n"
"X-Generator: Launchpad (build Unknown)\n"

#: lib/launchpad-integration.c:62
msgid "The Launchpad helper application failed"
msgstr "Ha fallado la aplicación asistente de Launchpad"

#: lib/launchpad-integration.c:65
#, c-format
msgid "The exit status was %d"
msgstr "El estado de salida fue %d"

#: lib/launchpad-integration.c:101
msgid "An error occurred while starting the Launchpad helper"
msgstr "Ocurrió un error al iniciar el asistente de Launchpad"

#: lib/launchpad-integration.c:139 lib/lpint-bonobo.c:49
msgid "Get Help Online..."
msgstr "Obtener ayuda en línea..."

#: lib/launchpad-integration.c:140
msgid "Connect to the Launchpad website for online help"
msgstr "Conectarse al sitio web de Launchpad para obtener ayuda en línea"

#: lib/launchpad-integration.c:143 lib/lpint-bonobo.c:50
msgid "Translate This Application..."
msgstr "Traducir esta aplicación..."

#: lib/launchpad-integration.c:144
msgid "Connect to the Launchpad website to help translate this application"
msgstr ""
"Conectarse al sitio web de Launchpad para ayudar a traducir esta aplicación"

#: lib/launchpad-integration.c:147 lib/lpint-bonobo.c:51
msgid "Report a Problem"
msgstr "Informar de un problema"
