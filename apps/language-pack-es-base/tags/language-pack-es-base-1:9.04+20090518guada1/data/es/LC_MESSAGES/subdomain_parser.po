# translation of apparmor-parser.po to
# Spanish message file for YaST2 (@memory@).
# Copyright (C) 2005 SUSE Linux Products GmbH.
# Copyright (C) 2002, 2003 SuSE Linux AG.
# Copyright (C) 1999, 2000, 2001 SuSE GmbH.
#
# Ibán josé García Castillo <Iban.Garcia@alufis35.uv.es>, 2000.
# Javier Moreno <javier.moreno@alufis35.uv.es>, 2000.
# Jordi Jaen Pallares <jordi@suse.de>, 1999, 2000, 2001.
# Pablo Iranzo Gómez <Pablo.Iranzo@uv.es>, 2000.
# Carlos E. Robinson <robin.listas@telefonica.net>, 2007.
# César Sánchez Alonso <csalinux@gmail.com>, 2007.
# Lluis Martinez <lmartinez@sct.ictnet.es>, 2008.
# Camaleón <noelamac@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: apparmor-parser\n"
"Report-Msgid-Bugs-To: apparmor-general@forge.novell.com\n"
"POT-Creation-Date: 2005-03-31 13:39-0800\n"
"PO-Revision-Date: 2009-04-08 17:38+0000\n"
"Last-Translator: Camaleón <Unknown>\n"
"Language-Team: Spanish <opensuse-translation-es@opensuse.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2009-04-11 12:48+0000\n"
"X-Generator: Launchpad (build Unknown)\n"

#: ../parser_include.c:80
msgid "Error couldn't allocate temporary file\n"
msgstr "Error no se pudo asignar el archivo temporal\n"

#: ../parser_include.c:103
msgid "Error: Out of Memory\n"
msgstr "Error: Sin memoria\n"

#: ../parser_include.c:122
#, c-format
msgid "Error: Can't add directory %s to search path\n"
msgstr "Error: No se puede añadir el directorio %s a la ruta de búsqueda\n"

#: ../parser_include.c:130
msgid "Error: Could not allocate memory\n"
msgstr "Error: No se puede asignar memoria\n"

#: ../parser_include.c:251
#, c-format
msgid "Error: could not allocate buffer for include. line %d in %s\n"
msgstr "Error: no pude asignar buffer para include. línea %d en %s\n"

#: ../parser_include.c:259 ../parser_include.c:274
#, c-format
msgid "Error: bad include. line %d in %s\n"
msgstr "Error: include erróneo. línea %d en %s\n"

#: ../parser_include.c:303
#, c-format
msgid "Error: exceeded %d levels of includes.  NOT processing %s include\n"
msgstr ""
"Error: superados %d niveles de incluidos.  NOT se procesan %s incluidos\n"

#: ../parser_include.c:318
#, c-format
msgid "Error: #include %s%c not found. line %d in %s\n"
msgstr "Error: #include %s%c no encontrado línea %d en %s\n"

#: ../parser_interface.c:115
#, c-format
msgid "PANIC bad increment buffer %p pos %p ext %p size %d res %p\n"
msgstr ""
"PÁNICO buffer de incremento incorrecto %p pos %p ext %p tamaño %d res %p\n"

#: ../parser_interface.c:505 ../parser_sysctl.c:95
#, c-format
msgid "Unable to open %s - %s\n"
msgstr "No se puede abrir %s - %s\n"

# clients/lan_nfs_write.ycp:78
# clients/lan_nfs_write.ycp:78
# include/nfs/nfs_write.ycp:54
# modules/Nfs.ycp:163
#: ../parser_interface.c:521
msgid "unable to create work area\n"
msgstr "no se puede crear un área de trabajo\n"

#: ../parser_interface.c:529
#, c-format
msgid "unable to serialize profile %s\n"
msgstr "no se puede serializar el perfil %s\n"

#: ../parser_interface.c:538
#, c-format
msgid "%s: Unable to write entire profile entry\n"
msgstr "%s: no se puede escribir la entrada del perfil completo\n"

#: parser_lex.l:345
#, c-format
msgid "(ip_mode) Found unexpected character: '%s'"
msgstr "(ip_mode) Se ha detectado un carácter inesperado: '%s'"

#: parser_lex.l:469
#, c-format
msgid "Found unexpected character: '%s'"
msgstr "Se ha detectado un carácter inesperado: '%s'"

#: ../parser_main.c:263
#, c-format
msgid "%s: Could not allocate memory for subdomain mount point\n"
msgstr ""
"%s: No se puede asignar memoria para un punto de montaje de subdominio\n"

#: ../parser_main.c:269
#, c-format
msgid "%s: Could not allocate memory for subdomainbase mount point\n"
msgstr ""
"%s: no se puede asignar memoria para el punto de montaje del subdominio "
"base\n"

#: ../parser_main.c:317
#, c-format
msgid ""
"%s: Sorry. You need root privileges to run this program.\n"
"\n"
msgstr ""
"%s: Lo sentimos. Necesita privilegios de superusuario para ejecutar este "
"programa.\n"
"\n"

#: ../parser_main.c:326
#, c-format
msgid ""
"%s: Warning! You've set this program setuid root.\n"
"Anybody who can run this program can update your AppArmor profiles.\n"
"\n"
msgstr ""
"%s: Advertencia: ha configurado el setuid de este programa como root.\n"
"Cualquiera que ejecute este programa podrá actualizar los perfiles de "
"AppArmor.\n"
"\n"

#: ../parser_main.c:376
#, c-format
msgid ""
"%s: Unable to query modules - '%s'\n"
"Either modules are disabled or your kernel is too old.\n"
msgstr ""
"%s: no es posible consultar los módulos '%s'\n"
"Los módulos están inhabilitados o el núcleo es demasiado antiguo.\n"

#: ../parser_main.c:382
#, c-format
msgid "%s: Unable to find "
msgstr "%s: no se ha encontrado "

#: ../parser_main.c:382
msgid ""
"!\n"
"Ensure that it has been loaded.\n"
msgstr ""
"!\n"
"Compruebe que se ha cargado.\n"

#: ../parser_main.c:411
#, c-format
msgid "%s: Errors found in file. Aborting.\n"
msgstr "%s: se han detectado errores en el archivo. Cancelando.\n"

#: ../parser_main.c:418
#, c-format
msgid "%s: Errors found in combining rules postprocessing. Aborting.\n"
msgstr ""
"%s: se han detectado errores en el posprocesado de combinación de reglas. "
"Cancelando.\n"

#: ../parser_main.c:426
#, c-format
msgid "%s: Errors found during regex postprocess. Aborting.\n"
msgstr "%s: Se encontraron errores en el postproceso de regex. Abortando.\n"

#: ../parser_merge.c:56
msgid "Couldn't merge entries. Out of Memory\n"
msgstr "No es posible fusionar las entradas. Memoria insuficiente\n"

#: ../parser_merge.c:77
#, c-format
msgid "profile %s: has merged rule %s with multiple x modifiers\n"
msgstr "perfil %s: ha fusionado la regla %s con varios modificadores x\n"

#: ../parser_merge.c:140
#, c-format
msgid "ERROR in profile %s, failed to load\n"
msgstr "ERROR en el perfil %s, error al cargar\n"

#: ../parser_sysctl.c:42
msgid "Bad write position\n"
msgstr "Posición de escritura incorrecta\n"

#: ../parser_sysctl.c:45
msgid "Permission denied\n"
msgstr "Permiso denegado\n"

#: ../parser_sysctl.c:48
msgid "Out of memory\n"
msgstr "Memoria insuficiente\n"

#: ../parser_sysctl.c:51
msgid "Couldn't copy profile Bad memory address\n"
msgstr "No se puede copiar el perfil; dirección de memoria incorrecta\n"

#: ../parser_sysctl.c:54
msgid "Profile doesn't conform to protocol\n"
msgstr "El perfil es no conforme al protocolo\n"

#: ../parser_sysctl.c:57
msgid "Profile does not match signature\n"
msgstr "El perfil no coincide con la firma\n"

#: ../parser_sysctl.c:60
msgid "Profile version not supported\n"
msgstr "Versión del perfil no soportada\n"

#: ../parser_sysctl.c:63
msgid "Profile already exists\n"
msgstr "El perfil ya existe\n"

#: ../parser_sysctl.c:66
msgid "Profile doesn't exist\n"
msgstr "El perfil no existe\n"

# include/network/summary.ycp:101
#: ../parser_sysctl.c:69
msgid "Unknown error\n"
msgstr "Error desconocido\n"

#: ../parser_sysctl.c:120
#, c-format
msgid "%s: Unable to add \"%s\".  "
msgstr "%s: no se puede añadir \"%s\".  "

#: ../parser_sysctl.c:126
#, c-format
msgid "%s: Unable to replace \"%s\".  "
msgstr "%s: no se puede sustituir \"%s\".  "

#: ../parser_sysctl.c:132
#, c-format
msgid "%s: Unable to remove \"%s\".  "
msgstr "%s: no se puede eliminar \"%s\".  "

#: ../parser_sysctl.c:138
#, c-format
msgid "%s: Unable to write to stdout\n"
msgstr "%s: no se puede escribir en stdout\n"

# clients/inst_source.ycp:191
# clients/inst_source.ycp:168
#: ../parser_sysctl.c:142 ../parser_sysctl.c:167
#, c-format
msgid "%s: ASSERT: Invalid option: %d\n"
msgstr "%s: AFIRMACIÓN: Opción no válida: %d\n"

#: ../parser_sysctl.c:153
#, c-format
msgid "Addition succeeded for \"%s\".\n"
msgstr "Éxito al añadir \"%s\".\n"

#: ../parser_sysctl.c:157
#, c-format
msgid "Replacement succeeded for \"%s\".\n"
msgstr "Sustitución realizada con éxito \"%s\".\n"

#: ../parser_sysctl.c:161
#, c-format
msgid "Removal succeeded for \"%s\".\n"
msgstr "Eliminación ejecutada con éxito de \"%s\".\n"

# classnames.ycp:65 classnames.ycp:68
# classnames.ycp:65 classnames.ycp:68
# classnames.ycp:65 classnames.ycp:68
#: parser_yacc.y:215 parser_yacc.y:240 parser_yacc.y:393 parser_yacc.y:409
#: parser_yacc.y:459 parser_yacc.y:500 parser_yacc.y:529 parser_yacc.y:543
#: parser_yacc.y:557 parser_yacc.y:571 parser_yacc.y:585 parser_yacc.y:613
#: parser_yacc.y:641 parser_yacc.y:678 parser_yacc.y:695 parser_yacc.y:709
#: parser_yacc.y:915
msgid "Memory allocation error."
msgstr "Error de asignación de memoria."

#: parser_yacc.y:259
#, c-format
msgid "Default allow subdomains are no longer supported, sorry. (domain: %s)"
msgstr ""
"Los subdominios permitidos por defecto no están soportados, lo sentimos. "
"(dominio: %s)"

#: parser_yacc.y:265
#, c-format
msgid ""
"Default allow subdomains are no longer supported, sorry. (domain: %s^%s)"
msgstr ""
"Los subdominios permitidos por defecto no están soportados, lo sentimos. "
"(dominio: %s^%s)"

#: parser_yacc.y:336
msgid "Assert: `rule' returned NULL."
msgstr "Afirmación: `rule' ha devuelto NULO."

#: parser_yacc.y:350
msgid "Assert: `netrule' returned NULL."
msgstr "Afirmación: `netrule' ha devuelto NULL."

#: parser_yacc.y:378
msgid "Assert: 'hat rule' returned NULL."
msgstr "Afirmación: 'hat rule' ha devuelto NULO."

#: parser_yacc.y:411
msgid "md5 signature given without execute privilege."
msgstr "firma md5 dada sin privilegio de ejecución."

#: parser_yacc.y:426 parser_yacc.y:435
#, c-format
msgid "missing an end of line character? (entry: %s)"
msgstr "¿Falta el carácter de final de línea? (entrada: %s)"

#: parser_yacc.y:441 parser_yacc.y:447
#, c-format
msgid ""
"Negative subdomain entries are no longer supported, sorry. (entry: %s)"
msgstr ""
"Las entradas negativas de subdominio no están soportadas, lo sentimos. "
"(entrada: %s)"

#: parser_yacc.y:495
msgid "Assert: `addresses' returned NULL."
msgstr "Afirmación: `addresses' ha devuelto NULL."

#: parser_yacc.y:595
msgid "Network entries can only have one TO address."
msgstr "Las entradas de red sólo pueden disponer de una dirección TO."

#: parser_yacc.y:600
msgid "Network entries can only have one FROM address."
msgstr "Las entradas de red sólo pueden disponer de una dirección FROM."

#: parser_yacc.y:618 parser_yacc.y:647
#, c-format
msgid "`%s' is not a valid ip address."
msgstr "`%s' no es una dirección IP válida."

#: parser_yacc.y:659
#, c-format
msgid "`/%d' is not a valid netmask."
msgstr "`/%d' no es una máscara de red válida."

#: parser_yacc.y:666
#, c-format
msgid "`%s' is not a valid netmask."
msgstr "`%s' no es una máscara de red válida."

#: parser_yacc.y:692 parser_yacc.y:712
#, c-format
msgid "ports must be between %d and %d"
msgstr "los puertos deben estar entre %d y %d"

#: parser_yacc.y:778
#, c-format
msgid "AppArmor parser error, line %d: %s\n"
msgstr "Error del analizador AppArmor, línea %d: %s\n"

#: parser_yacc.y:821
msgid "Exec qualifier 'i' must be followed by 'x'"
msgstr "El calificador de ejecución 'i' debe estar seguido de 'x'"

#: parser_yacc.y:823
msgid "Exec qualifier 'i' invalid, conflicting qualifier already specified"
msgstr ""
"El calificador de ejecución 'i' no es válido, entra en conflicto con un "
"calificador ya definido"

#: parser_yacc.y:833
msgid "Exec qualifier 'u' must be followed by 'x'"
msgstr "El calificador de ejecución \"u\" debe seguirse de \"x\""

#: parser_yacc.y:835
msgid "Exec qualifier 'u' invalid, conflicting qualifier already specified"
msgstr ""

#: parser_yacc.y:845
msgid "Exec qualifier 'p' must be followed by 'x'"
msgstr "El calificador de ejecución \"p\" debe seguirse de \"x\""

#: parser_yacc.y:847
msgid "Exec qualifier 'p' invalid, conflicting qualifier already specified"
msgstr ""

#: parser_yacc.y:856
msgid "Invalid mode, 'x' must be preceded by exec qualifier 'i', 'u' or 'p'"
msgstr ""
"Modo inválido,\"x\" debe estar precedido por el calificador de ejecución "
"'i', 'u' o 'p'"

#: parser_yacc.y:860
msgid "Internal: unexpected mode character in input"
msgstr "Interno: carácter de modo inesperado en la entrada"

#: parser_yacc.y:1211
#, c-format
msgid "%s: Illegal open {, nesting groupings not allowed\n"
msgstr ""
"%s: { de apertura ilegal, el anidamiento de grupos no está permitido\n"

#: parser_yacc.y:1231
#, c-format
msgid "%s: Regex grouping error: Invalid number of items between {}\n"
msgstr ""
"%s: error de agrupación regex: número de elementos entre {} no válido\n"

#: parser_yacc.y:1237
#, c-format
msgid ""
"%s: Regex grouping error: Invalid close }, no matching open { detected\n"
msgstr ""
"%s: error de agrupación regex: cierre } no válido, no se ha encontrado el "
"signo de apertura { correspondiente\n"

#: parser_yacc.y:1294
#, c-format
msgid "%s: Regex grouping error: Unclosed grouping, expecting close }\n"
msgstr ""

#: parser_yacc.y:1309
#, c-format
msgid "%s: Internal buffer overflow detected, %d characters exceeded\n"
msgstr ""
"%s: detectado desbordamiento de buffer interno, superado en %d caracteres\n"

#: parser_yacc.y:1314
#, c-format
msgid "%s: Unable to parse input line '%s'\n"
msgstr "%s: no es posible analizar la línea de entrada '%s'\n"

#: parser_yacc.y:1348
#, c-format
msgid "%s: Failed to compile regex '%s' [original: '%s']\n"
msgstr "%s: error al compilar regex '%s' [original: '%s']\n"

# include/partitioning/raid_ui.ycp:341
# include/partitioning/raid_ui.ycp:341
# include/partitioning/raid_ui.ycp:341
#: parser_yacc.y:1353
#, c-format
msgid "%s: error near               "
msgstr "%s: error aproximado               "

#: parser_yacc.y:1364
#, c-format
msgid "%s: error reason: '%s'\n"
msgstr "%s: motivo del error: '%s'\n"

#: parser_yacc.y:1374
#, c-format
msgid "%s: Failed to compile regex '%s' [original: '%s'] - malloc failed\n"
msgstr ""
"%s: error al compilar regex '%s' [original: '%s'] - error de malloc\n"

#: parser_yacc.y:1424
#, c-format
msgid "%s: Subdomain '%s' defined, but no parent '%s'.\n"
msgstr "%s: Subdominio '%s' definido, pero no padre '%s'.\n"

#: parser_yacc.y:1461
#, c-format
msgid "%s: Two SubDomains defined for '%s'.\n"
msgstr "%s: Dos subdominios definidos para '%s'.\n"

#: ../parser.h:37
#, c-format
msgid "Warning (line %d): "
msgstr "Aviso (línea %d): "
